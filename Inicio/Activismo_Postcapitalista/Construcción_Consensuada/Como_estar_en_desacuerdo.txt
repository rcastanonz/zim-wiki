Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-08-12T16:25:12-05:00

====== Como estar en desacuerdo ======
Created lunes 12 agosto 2019

DH0. Insulto («Name-calling»)

Esta es una de las formas mas bajas de desacuerdo, y probablemente también, la más común. Todos hemos visto comentarios como:

	¡¡¡eres un maricón!!!

Pero es importante darse cuenta que insultos más articulados tienen tan poco peso como este. Un comentario como

	El autor tiene conocimiento muy superficial y es un ególatra.

no es más que una versión pretenciosa de «¡¡¡eres un maricón!!!»

DH1. Ad Hominem:

Un ataque ad hominem no es tan débil como el mero insulto. Incluso puede que tenga un poco de peso. Por ejemplo, si un senador escribe un artículo diciendo que los sueldos de los senadores debería ser incrementados, uno podría responder

	Por supuesto que el dice eso. Es un senador.

Esto no refuta el argumento del autor, pero al menos es relevante a la discusión. Sin embargo, todavía es una forma muy débil de desacuerdo. Si hay algo errado en el argumento del senador, uno debería decir lo que es; y si no hay nada errado, ¿que diferencia hace que él sea senador?

Decir que el autor no tiene la autoridad para escribir sobre un tópico es una variante de ad hominem – y una forma particularmente inútil, porque las buenas ideas frecuentemente se originan en gente que vienen de otros campos. La pregunta es si el autor tiene razón o no. Si la falta de autoridad le causó el cometer errores, apunta cuáles son. Y si no lo hizo, entonces no hay problema.

DH2. Respondiendo al Tono.

En el siguiente nivel comenzamos a ver respuestas a lo que se ha escrito en vez de al escritor. La forma más baja de estos niveles es el estar en desacuerdo con el tono del autor, p.e.

	No puedo creer que el autor desestime el Diseño Inteligente en una forma tan poco responsable.

Aunque es mejor que atacar al autor, esta es todavía una forma muy débil de desacuerdo. Importa mucho más si el autor tiene razón o no que cuál es su tono. Especialmente porque el tono es tán difícil de juzgar. Alguien que tiene un problema con algún tema puede ofenderse por el tono que otros lectores pueden encontrar neutral.

Así que si lo peor que uno puede decir sobre algo es criticar su tono, uno no está diciendo mucho. ¿Está delirando el autor, pero está en lo correcto? Mejor esto que ser serio pero estár equivocado. Y si el autor no está en lo correcto, menciona en qué.

DH3. Contradicción.

En esta etapa finalmente obtenemos respuestas a lo que fue dicho, en vez de como o quién lo dijo. La forma más baja de respuesta a un argumento es simplemente plantear el caso opuesto, con poca o ninguna evidencia que lo apoye.

Esto es frecuentemente combinado con frases DH2 como

	No puedo creer que el autor desestima el Diseño Inteligente en una forma tan irresponsable. El diseño inteligente es una teoría científica legítima.

Contradicción puede tener peso a veces. A veces el mero hecho de ver el caso opuesto en forma explícitamente es suficiente para ver que es correcto. Pero habitualmente la evidencia ayuda.

DH4. Contraargumento:

En el nivel 4 llegamos a la primera forma de desacuerdo convincente: el contraargumento. Las formas previas pueden ser ignoradas ya que no prueban nada. Contraargumento puede probar algo. El problema es que es difícil saber exactamente qué.

Contraargumento es contradicción más razonamiento y/o evidencia. Cuando es apuntado directamente el argumento original, puede ser convincente. Pero desafortunadamente es común que los contraargumentos sean dirigidos a algo ligeramente distinto. Más a menudo que no, dos personas discutiendo apasionadamente sobre algo de hecho están discutiendo sobre dos cosas distintas. A veces incluso están de acuerdo el uno con el otro, pero están tan inmersos en su disputa que no se dan cuenta.

Podría haber una razón legítima para argumentar contra algo ligeramente distinta a lo que el autor original dijo: cuando uno cree que se le escapo lo más importante del tema sobre el que escribió. Pero cuando uno hace eso, debería decirlo explícitamente.

DH5. Refutación.

La forma más convincente de desacuerdo es refutación. Es también la más rara, porque es la que requiere más esfuerzo. De hecho, la jerarquía de desacuerdo forma una especie de pirámide, en el sentido que mientrás más arriba uno va las menos instancias encuentra.

Para refutar a alguien uno probablemente tendría que citarlos. Uno tiene que encontrar una «pistola humeante», un párrafo con el que uno está en desacuerdo y que cree equivocado. Si uno no puede encontrar una cita con la que está en desacuerdo, puede que uno esté en desacuerdo con un espantapájaros.

Aunque en general la refutación requiere citación, citación no necesariamente implica refutación. Algunos escritores citan parte de cosas con la que están en desacuerdo para dar la impresión de refutación legítima, y después proceden con una respuesta tan baja como DH3 o incluso DH0.

DH6. Refutar el Punto Central.

La fuerza de una refutación depende de qué estás refutando. La forma más poderosa de refutación es refutar el punto central de alguien.

Incluso en formas tan altas como DH5 todavía vemos deshonestidad deliberada, como en el caso cuando alguien elije puntos menores de un argumento y los refuta. A veces el espíritu con que esto se hace lo convierte más en una forma más sofisticada de ad hominem que una refutación real. Por ejemplo, corregir la gramática de alguien, o insistir en en apuntar errores menores en nombres o números. Aunque el argumento opuesto depende en esas cosas, el único propósito de corregirlos es desacreditar al oponente.

Refutar algo de verdad requiere que uno refute el punto centrar, o al menos uno de ellos. Y eso significa que uno tiene que comprometerse explicitamente a cuál es el punto central. Por lo tanto, una refutación realmente efectiva sería algo como

	El punto principal del autor parece ser x. Como el dice:

		<cita del artículo original>

	Pero esto esta equivocado por las siguientes razones….

La cita que uno destaca como errada no necesita ser el punto principal del autor. Es suficiente refutar algo sobre lo que este punto depende.

Que Significa

Ahora tenemos una forma de clasificar formas de desacuerdo. ¿Para que sirve? Una cosa que la jerarquía de desacuerdo no nos da es una forma de elegir un ganador. Una respuesta DH6 puede ser completamente errada.

Pero aunque niveles DH no establecen un límite mínimo sobre cuán convincente es una respuesta, si establecen un límite máximo. Una respuesta DH6 puede ser poco convincente, pero una DH2 o más baja siempre es poco convincente.

La ventaja más obvia de clasificar las formas de desacuerdo es que ayudará a la gente a evaluar lo que leen. En particular, los ayudará a ver más allá los argumentos más intelectualmente deshonestos. Un expositor o escritor elocuente puede dar la impresión de vencer a un oponente meramente usando palabras fuertes. De hecho esta es probablemente la característica que define a un demagogo. Al darle nombres a las distintas formas de desacuerdo le damos a los lectores críticos una forma de ver esos problemas.

Esas etiquetas también pueden ayudar a otros escritores. La mayoría de la deshonestidad intelectual es sin intención. Alguien que está argumentando contra el tono de algo con lo que está en desacuerdo puede creer que realmente está diciendo algo. Dar un par de pasos atrás y ver su posición en la jerarquía de desacuerdos puede inspirarlo a tratar de moverse más arriba a contraargumentar o refutar.

Pero el beneficio más grande de estar en desacuerdo bien no es solo que hará que las conversaciones sean mejores, sino que hará más feliz a la gente que las tienen. Si tú estudia conversaciones, encontrarás que hay mucha más crueldad en DH1 que más arriba en DH6. Tú no tienes que ser cruel cuando tienes algo real que decir. De hecho, no quieres ser cruel. Si tienes algo real que decir, el ser cruel se convierte en un obstáculo.

Si moverse más arriba en la jerarquía hace que la gente sea menos cruel, eso hará que la mayoría de ellos también sean más felices. La mayoría de la gente no disfruta ser cruel; lo hacen porque no pueden evitarlo.
