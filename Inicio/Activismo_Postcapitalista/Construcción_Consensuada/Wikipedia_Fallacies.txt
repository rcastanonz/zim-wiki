Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-11-21T22:06:17-05:00

====== Wikipedia Fallacies ======
Creado jueves 21 noviembre 2019

List of fallacies
From Wikipedia, the free encyclopedia
Jump to navigationJump to search
For specific popular misconceptions, see List of common misconceptions.
In reasoning to argue a claim, a fallacy is reasoning that is evaluated as logically incorrect and that undermines the logical validity of the argument and permits its recognition as unsound. Regardless of their soundness, all registers and manners of speech can demonstrate fallacies.

Because of their variety of structure and application, fallacies are challenging to classify so as to satisfy all practitioners. Fallacies can be classified strictly by either their structure or content, such as classifying them as formal fallacies or informal fallacies, respectively. The classification of informal fallacies may be subdivided into categories such as linguistic, relevance through omission, relevance through intrusion, and relevance through presumption.[1] On the other hand, fallacies may be classified by the process by which they occur, such as material fallacies (content), verbal fallacies (linguistic), and again formal fallacies (error in inference). In turn, material fallacies may be placed into the more general category of informal fallacies, while formal fallacies may be clearly placed into the more precise category of logical (deductive) fallacies.[clarification needed] Yet, verbal fallacies may be placed into either informal or deductive classifications; compare equivocation which is a word or phrase based ambiguity (e.g., "he is mad", which may refer to either him being angry or clinically insane) to the fallacy of composition which is premise and inference based ambiguity (e.g., "this must be a good basketball team because each of its members is an outstanding player").[2]

The conscious or habitual use of fallacies as rhetorical devices is prevalent in the desire to persuade when the focus is more on communication and eliciting common agreement rather than on the correctness of the reasoning. The effective use of a fallacy by an orator may be considered clever, but by the same token, the reasoning of that orator should be recognized as unsound, and thus the orator's claim, supported by an unsound argument, will be regarded as unfounded and dismissed.[3]


Contents
1	Formal fallacies
1.1	Propositional fallacies
1.2	Quantification fallacies
1.3	Formal syllogistic fallacies
2	Informal fallacies
2.1	Improper premise
2.2	Faulty generalizations
2.3	Questionable cause
2.4	Relevance fallacies
2.4.1	Red herring fallacies
3	See also
4	References
4.1	Citations
4.2	Sources
5	Further reading
6	External links
Formal fallacies
Main article: Formal fallacy
A formal fallacy is an error in logic that can be seen in the argument's form.[4] All formal fallacies are specific types of non sequitur.

Appeal to probability – a statement that takes something for granted because it would probably be the case (or might be the case).[5][6]
Argument from fallacy (also known as the fallacy fallacy) – the assumption that if an argument for some conclusion is fallacious, then the conclusion is false.[7]
Base rate fallacy – making a probability judgment based on conditional probabilities, without taking into account the effect of prior probabilities.[8]
Conjunction fallacy – the assumption that an outcome simultaneously satisfying multiple conditions is more probable than an outcome satisfying a single one of them.[9]
Masked-man fallacy (illicit substitution of identicals) – the substitution of identical designators in a true statement can lead to a false one.[10]
Propositional fallacies
A propositional fallacy is an error in logic that concerns compound propositions. For a compound proposition to be true, the truth values of its constituent parts must satisfy the relevant logical connectives that occur in it (most commonly: [and], [or], [not], [only if], [if and only if]). The following fallacies involve inferences whose correctness is not guaranteed by the behavior of those logical connectives, and hence, which are not logically guaranteed to yield true conclusions.
Types of propositional fallacies:

Affirming a disjunct – concluding that one disjunct of a logical disjunction must be false because the other disjunct is true; A or B; A, therefore not B.[11]
Affirming the consequent – the antecedent in an indicative conditional is claimed to be true because the consequent is true; if A, then B; B, therefore A.[11]
Denying the antecedent – the consequent in an indicative conditional is claimed to be false because the antecedent is false; if A, then B; not A, therefore not B.[11]
Quantification fallacies
A quantification fallacy is an error in logic where the quantifiers of the premises are in contradiction to the quantifier of the conclusion.
Types of quantification fallacies:

Existential fallacy – an argument that has a universal premise and a particular conclusion.[12]
Formal syllogistic fallacies
Syllogistic fallacies – logical fallacies that occur in syllogisms.

Affirmative conclusion from a negative premise (illicit negative) – a categorical syllogism has a positive conclusion, but at least one negative premise.[12]
Fallacy of exclusive premises – a categorical syllogism that is invalid because both of its premises are negative.[12]
Fallacy of four terms (quaternio terminorum) – a categorical syllogism that has four terms.[13]
Illicit major – a categorical syllogism that is invalid because its major term is not distributed in the major premise but distributed in the conclusion.[12]
Illicit minor – a categorical syllogism that is invalid because its minor term is not distributed in the minor premise but distributed in the conclusion.[12]
Negative conclusion from affirmative premises (illicit affirmative) – a categorical syllogism has a negative conclusion but affirmative premises.[12]
Fallacy of the undistributed middle – the middle term in a categorical syllogism is not distributed.[14]
Modal fallacy – confusing possibility with necessity.
Modal scope fallacy – a degree of unwarranted necessity is placed in the conclusion.
Informal fallacies
Main article: Informal fallacy
Informal fallacies – arguments that are fallacious for reasons other than structural (formal) flaws and usually require examination of the argument's content.[15]

Argument to moderation (false compromise, middle ground, fallacy of the mean, argumentum ad temperantiam) – assuming that the compromise between two positions is always correct.[16]
Continuum fallacy (fallacy of the beard, line-drawing fallacy, sorites fallacy, fallacy of the heap, bald man fallacy) – improperly rejecting a claim for being imprecise.[17]
Correlative-based fallacies
Suppressed correlative – a correlative is redefined so that one alternative is made impossible.[18]
Definist fallacy – involves the confusion between two notions by defining one in terms of the other.[19]
Divine fallacy (argument from incredulity) – arguing that, because something is so incredible or amazing, it must be the result of superior, divine, alien or paranormal agency.[20]
Double counting – counting events or occurrences more than once in probabilistic reasoning, which leads to the sum of the probabilities of all cases exceeding unity.
Equivocation – the misleading use of a term with more than one meaning (by glossing over which meaning is intended at a particular time).[21]
Ambiguous middle term – a common ambiguity in syllogisms in which the middle term is equivocated.[22]
Definitional retreat – changing the meaning of a word to deal with an objection raised against the original wording.[1]
Motte-and-bailey fallacy – the arguer conflates two positions with similar properties, one modest and easy to defend (the "motte") and one much more controversial (the "bailey"). The arguer advances the controversial position, but when challenged, they insist that they are only advancing the more modest position.[23][24][25]
Fallacy of accent – a specific type of ambiguity that arises when the meaning of a sentence is changed by placing an unusual prosodic stress, or when, in a written passage, it is left unclear which word the emphasis was supposed to fall on.
Persuasive definition – a form of stipulative definition which purports to describe the "true" or "commonly accepted" meaning of a term, while in reality stipulating an uncommon or altered use.
(See also the if-by-whiskey fallacy, below)
Ecological fallacy – inferences about the nature of specific individuals are based solely upon aggregate statistics collected for the group to which those individuals belong.[26]
Etymological fallacy – reasoning that the original or historical meaning of a word or phrase is necessarily similar to its actual present-day usage.[27]
Fallacy of composition – assuming that something true of part of a whole must also be true of the whole.[28]
Fallacy of division – assuming that something true of a thing must also be true of all or some of its parts.[29]
False attribution – an advocate appeals to an irrelevant, unqualified, unidentified, biased or fabricated source in support of an argument.
Fallacy of quoting out of context (contextotomy, contextomy; quotation mining) – refers to the selective excerpting of words from their original context in a way that distorts the source's intended meaning.[30]
False authority (single authority) – using an expert of dubious credentials or using only one opinion to sell a product or idea. Related to the appeal to authority (not always fallacious).
False dilemma (false dichotomy, fallacy of bifurcation, black-or-white fallacy) – two alternative statements are held to be the only possible options when in reality there are more.[31]
False equivalence – describing a situation of logical and apparent equivalence, when in fact there is none.
Feedback fallacy - in the context of performance appraisal, the belief in the accuracy of feedback, despite evidence that feedback is subject to large systematic errors due to the idiosyncratic rater effect.[32]
Historian's fallacy – the assumption that decision makers of the past viewed events from the same perspective and had the same information as those subsequently analyzing the decision.[33] (Not to be confused with presentism, which is a mode of historical analysis in which present-day ideas, such as moral standards, are projected into the past.)
Historical fallacy – a set of considerations is thought to hold good only because a completed process is read into the content of the process which conditions this completed result.[34]
Baconian fallacy - using pieces of historical evidence without the aid of specific methods, hypotheses, or theories in an attempt to make a general truth about the past. Commits historians "to the pursuit of an impossible object by an impracticable method".[35]
Homunculus fallacy – a "middle-man" is used for explanation; this sometimes leads to regressive middle-men. Explains without actually explaining the real nature of a function or a process. Instead, it explains the concept in terms of the concept itself, without first defining or explaining the original concept. Explaining thought as something produced by a little thinker, a sort of homunculus inside the head, merely explains it as another kind of thinking (as different but the same).[36]
Inflation of conflict – arguing that if experts of a field of knowledge disagree on a certain point, the experts must know nothing, and therefore no conclusion can be reached, or that the legitimacy of their entire field is put to question.[37]
If-by-whiskey – an argument that supports both sides of an issue by using terms that are selectively emotionally sensitive.
Incomplete comparison – insufficient information is provided to make a complete comparison.
Inconsistent comparison – different methods of comparison are used, leaving a false impression of the whole comparison.
Intentionality fallacy – the insistence that the ultimate meaning of an expression must be consistent with the intention of the person from whom the communication originated (e.g. a work of fiction that is widely received as a blatant allegory must necessarily not be regarded as such if the author intended it not to be so.)[38]
Kettle logic – using multiple, jointly inconsistent arguments to defend a position.[dubious – discuss]
Ludic fallacy – the belief that the outcomes of non-regulated random occurrences can be encapsulated by a statistic; a failure to take into account that unknown unknowns in determining the probability of events taking place.[39]
McNamara fallacy (quantitative fallacy) – making a decision based only on quantitative observations, discounting all other considerations.
Mind projection fallacy – subjective judgments are "projected" to be inherent properties of an object, rather than being related to personal perceptions of that object.
Moralistic fallacy – inferring factual conclusions from purely evaluative premises in violation of fact–value distinction. For instance, inferring is from ought is an instance of moralistic fallacy. Moralistic fallacy is the inverse of naturalistic fallacy defined below.
Moving the goalposts (raising the bar) – argument in which evidence presented in response to a specific claim is dismissed and some other (often greater) evidence is demanded.
Nirvana fallacy (perfect-solution fallacy) – solutions to problems are rejected because they are not perfect.
Proof by assertion – a proposition is repeatedly restated regardless of contradiction; sometimes confused with argument from repetition (argumentum ad infinitum, argumentum ad nauseam)
Prosecutor's fallacy – a low probability of false matches does not mean a low probability of some false match being found.
Proving too much – using a form of argument that, if it were valid, could be used to reach an additional, invalid conclusion.
Psychologist's fallacy – an observer presupposes the objectivity of their own perspective when analyzing a behavioral event.
Referential fallacy[40] – assuming all words refer to existing things and that the meaning of words reside within the things they refer to, as opposed to words possibly referring to no real object or that the meaning of words often comes from how they are used.
Reification (concretism, hypostatization, or the fallacy of misplaced concreteness) – a fallacy of ambiguity, when an abstraction (abstract belief or hypothetical construct) is treated as if it were a concrete, real event or physical entity. In other words, it is the error of treating as a "real thing" something that is not a real thing, but merely an idea.
Retrospective determinism – the argument that because an event has occurred under some circumstance, the circumstance must have made its occurrence inevitable.
Slippery slope (thin edge of the wedge, camel's nose) – asserting that a relatively small first step inevitably leads to a chain of related events culminating in some significant impact/event that should not happen, thus the first step should not happen. It is, in its essence, an appeal to probability fallacy.[41]
Special pleading – a proponent of a position attempts to cite something as an exemption to a generally accepted rule or principle without justifying the exemption.
Improper premise
Begging the question (petitio principii) – providing what is essentially the conclusion of the argument as a premise.[42][43][44]
Loaded label – while not inherently fallacious, use of evocative terms to support a conclusion is a type of begging the question fallacy. When fallaciously used, the term's connotations are relied on to sway the argument towards a particular conclusion. For example, an organic foods advertisement that says "Organic foods are safe and healthy foods grown without any pesticides, herbicides, or other unhealthy additives." Use of the term "unhealthy additives" is used as support for the idea that the product is safe.[45]
Circular reasoning (circulus in demonstrando) – the reasoner begins with what he or she is trying to end up with; sometimes called "assuming the conclusion".
Fallacy of many questions (complex question, fallacy of presuppositions, loaded question, plurium interrogationum) – someone asks a question that presupposes something that has not been proven or accepted by all the people involved. This fallacy is often used rhetorically so that the question limits direct replies to those that serve the questioner's agenda.
Faulty generalizations
Faulty generalization – reach a conclusion from weak premises. Unlike fallacies of relevance, in fallacies of defective induction, the premises are related to the conclusions yet only weakly support the conclusions. A faulty generalization is thus produced.

Accident – an exception to a generalization is ignored.[46]
No true Scotsman – makes a generalization true by changing the generalization to exclude a counterexample.[47]
Cherry picking (suppressed evidence, incomplete evidence) – act of pointing at individual cases or data that seem to confirm a particular position, while ignoring a significant portion of related cases or data that may contradict that position.[48]
Survivorship bias – a small number of successes of a given process are actively promoted while completely ignoring a large number of failures
False analogy – an argument by analogy in which the analogy is poorly suited.[49]
Hasty generalization (fallacy of insufficient statistics, fallacy of insufficient sample, fallacy of the lonely fact, hasty induction, secundum quid, converse accident, jumping to conclusions) – basing a broad conclusion on a small sample or the making of a determination without all of the information required to do so.[50]
Inductive fallacy – A more general name to some fallacies, such as hasty generalization. It happens when a conclusion is made of premises that lightly support it.
Misleading vividness – involves describing an occurrence in vivid detail, even if it is an exceptional occurrence, to convince someone that it is a problem; this also relies on the appeal to emotion fallacy.
Overwhelming exception – an accurate generalization that comes with qualifications that eliminate so many cases that what remains is much less impressive than the initial statement might have led one to assume.[51]
Thought-terminating cliché – a commonly used phrase, sometimes passing as folk wisdom, used to quell cognitive dissonance, conceal lack of forethought, move on to other topics, etc. – but in any case, to end the debate with a cliché rather than a point.
Questionable cause
Questionable cause - Is a general type error with many variants. Its primary basis is the confusion of association with causation. Either by inappropriately deducing (or rejecting) causation or a broader failure to properly investigate the cause of an observed effect.

Cum hoc ergo propter hoc (Latin for "with this, therefore because of this"; correlation implies causation; faulty cause/effect, coincidental correlation, correlation without causation) – a faulty assumption that, because there is a correlation between two variables, one caused the other.[52]
Post hoc ergo propter hoc (Latin for "after this, therefore because of this"; temporal sequence implies causation) – X happened, then Y happened; therefore X caused Y.[53]
Wrong direction (reverse causation) – cause and effect are reversed. The cause is said to be the effect and vice versa.[54] The consequence of the phenomenon is claimed to be its root cause.
Ignoring a common cause
Fallacy of the single cause (causal oversimplification[55]) – it is assumed that there is one, simple cause of an outcome when in reality it may have been caused by a number of only jointly sufficient causes.
Furtive fallacy – outcomes are asserted to have been caused by the malfeasance of decision makers.
Gambler's fallacy – the incorrect belief that separate, independent events can affect the likelihood of another random event. If a fair coin lands on heads 10 times in a row, the belief that it is "due to the number of times it had previously landed on tails" is incorrect.[56]
Inverse gambler's fallacy
Magical thinking – fallacious attribution of causal relationships between actions and events. In anthropology, it refers primarily to cultural beliefs that ritual, prayer, sacrifice, and taboos will produce specific supernatural consequences. In psychology, it refers to an irrational belief that thoughts by themselves can affect the world or that thinking something corresponds with doing it.
Regression fallacy – ascribes cause where none exists. The flaw is failing to account for natural fluctuations. It is frequently a special kind of post hoc fallacy.
Relevance fallacies
Appeal to the stone (argumentum ad lapidem) – dismissing a claim as absurd without demonstrating proof for its absurdity.[57]
Argument from ignorance (appeal to ignorance, argumentum ad ignorantiam) – assuming that a claim is true because it has not been or cannot be proven false, or vice versa.[58]
Argument from incredulity (appeal to common sense) – "I cannot imagine how this could be true; therefore, it must be false."[59]
Argument from repetition (argumentum ad nauseam, argumentum ad infinitum) – repeating an argument until nobody cares to discuss it any more;[60][61] sometimes confused with proof by assertion
Argument from silence (argumentum ex silentio) – assuming that a claim is true based on the absence of textual or spoken evidence from an authoritative source, or vice versa.[62]
Ignoratio elenchi (irrelevant conclusion, missing the point) – an argument that may in itself be valid, but does not address the issue in question.[63]
Red herring fallacies
A red herring fallacy, one of the main subtypes of fallacies of relevance, is an error in logic where a proposition is, or is intended to be, misleading in order to make irrelevant or false inferences. In the general case any logical inference based on fake arguments, intended to replace the lack of real arguments or to replace implicitly the subject of the discussion.[64][65]

Red herring – a speaker attempts to distract an audience by deviating from the topic at hand by introducing a separate argument the speaker believes is easier to speak to.[66] Argument given in response to another argument, which is irrelevant and draws attention away from the subject of argument. See also irrelevant conclusion.

Ad hominem – attacking the arguer instead of the argument.
Circumstantial ad hominem - stating that the arguer's personal situation or perceived benefit from advancing a conclusion means that their conclusion is wrong.[67]
Poisoning the well – a subtype of ad hominem presenting adverse information about a target person with the intention of discrediting everything that the target person says.[68]
Appeal to motive – dismissing an idea by questioning the motives of its proposer.
Kafka-trapping – a sophistical and unfalsifiable form of argument that attempts to overcome an opponent by inducing a sense of guilt and using the opponent's denial of guilt as further evidence of guilt.[69]
Tone policing – focusing on emotion behind (or resulting from) a message rather than the message itself as a discrediting tactic.
Traitorous critic fallacy (ergo decedo, 'thus leave') – a critic's perceived affiliation is portrayed as the underlying reason for the criticism and the critic is asked to stay away from the issue altogether. Easily confused with the association fallacy ("guilt by association") below.
Appeal to authority (argument from authority, argumentum ad verecundiam) – an assertion is deemed true because of the position or authority of the person asserting it.[70][71]
Appeal to accomplishment – an assertion is deemed true or false based on the accomplishments of the proposer. This may often also have elements of appeal to emotion (see below).
Courtier's reply – a criticism is dismissed by claiming that the critic lacks sufficient knowledge, credentials, or training to credibly comment on the subject matter.
Appeal to consequences (argumentum ad consequentiam) – the conclusion is supported by a premise that asserts positive or negative consequences from some course of action in an attempt to distract from the initial discussion.[72]
Appeal to emotion – an argument is made due to the manipulation of emotions, rather than the use of valid reasoning.[73]
Appeal to fear – an argument is made by increasing fear and prejudice towards the opposing side[74]
Appeal to flattery – an argument is made due to the use of flattery to gather support.[75]
Appeal to pity (argumentum ad misericordiam) – an argument attempts to induce pity to sway opponents.[76]
Appeal to ridicule – an argument is made by incorrectly presenting the opponent's argument in a way that makes it appear ridiculous.[77]
Appeal to spite – an argument is made through exploiting people's bitterness or spite towards an opposing party.[78]
Judgmental language – insulting or pejorative language to influence the audience's judgment.
Pooh-pooh – dismissing an argument perceived unworthy of serious consideration.[79]
Wishful thinking – a decision is made according to what might be pleasing to imagine, rather than according to evidence or reason.[80]
Appeal to nature – judgment is based solely on whether the subject of judgment is 'natural' or 'unnatural'.[81] (Sometimes also called the "naturalistic fallacy", but is not to be confused with the other fallacies by that name.)
Appeal to novelty (argumentum novitatis, argumentum ad antiquitatis) – a proposal is claimed to be superior or better solely because it is new or modern.[82]
Appeal to poverty (argumentum ad Lazarum) – supporting a conclusion because the arguer is poor (or refuting because the arguer is wealthy). (Opposite of appeal to wealth.)[83]
Appeal to tradition (argumentum ad antiquitatem) – a conclusion supported solely because it has long been held to be true.[84]
Appeal to wealth (argumentum ad crumenam) – supporting a conclusion because the arguer is wealthy (or refuting because the arguer is poor).[85] (Sometimes taken together with the appeal to poverty as a general appeal to the arguer's financial situation.)
Argumentum ad baculum (appeal to the stick, appeal to force, appeal to threat) – an argument made through coercion or threats of force to support position.[86]
Argumentum ad populum (appeal to widespread belief, bandwagon argument, appeal to the majority, appeal to the people) – a proposition is claimed to be true or good solely because a majority or many people believe it to be so.[87]
Association fallacy (guilt by association and honor by association) – arguing that because two things share (or are implied to share) some property, they are the same.[88]
Ipse dixit (bare assertion fallacy) – a claim that is presented as true without support, as self-evidently true, or as dogmatically true. This fallacy relies on the implied expertise of the speaker or on an unstated truism.[89][90]
Bulverism (psychogenetic fallacy) – inferring why an argument is being used, associating it to some psychological reason, then assuming it is invalid as a result. The assumption that if the origin of an idea comes from a biased mind, then the idea itself must also be a falsehood.[37]
Chronological snobbery – a thesis is deemed incorrect because it was commonly held when something else, known to be false, was also commonly held.[91][92]
Fallacy of relative privation (also known as "appeal to worse problems" or "not as bad as") – dismissing an argument or complaint due to the existence of more important problems in the world, regardless of whether those problems bear relevance to the initial argument. First World problems are a subset of this fallacy.[93]
Genetic fallacy – a conclusion is suggested based solely on something or someone's origin rather than its current meaning or context.[94]
I'm entitled to my opinion – a person discredits any opposition by claiming that they are entitled to their opinion.
Moralistic fallacy – inferring factual conclusions from evaluative premises, in violation of fact-value distinction; e.g. making statements about what is, on the basis of claims about what ought to be. This is the inverse of the naturalistic fallacy.
Naturalistic fallacy – inferring evaluative conclusions from purely factual premises[95][96] in violation of fact-value distinction. Naturalistic fallacy (sometimes confused with appeal to nature) is the inverse of moralistic fallacy.
Is–ought fallacy[97] – statements about what is, on the basis of claims about what ought to be.
Naturalistic fallacy fallacy[98] (anti-naturalistic fallacy)[99] – inferring an impossibility to infer any instance of ought from is from the general invalidity of is-ought fallacy, mentioned above. For instance, is {\displaystyle P\lor \neg P}P \lor \neg P does imply ought {\displaystyle P\lor \neg P}P \lor \neg P for any proposition {\displaystyle P}P, although the naturalistic fallacy fallacy would falsely declare such an inference invalid. Naturalistic fallacy fallacy is a type of argument from fallacy.
Straw man fallacy – an argument based on misrepresentation of an opponent's position.[100]
Texas sharpshooter fallacy – improperly asserting a cause to explain a cluster of data.[101]
Tu quoque ('you too' – appeal to hypocrisy, whataboutism) – the argument states that a certain position is false or wrong or should be disregarded because its proponent fails to act consistently in accordance with that position.[102]
Two wrongs make a right – occurs when it is assumed that if one wrong is committed, another wrong will rectify it.[103]
Vacuous truth – a claim that is technically true but meaningless, in the form of claiming that no A in B has C, when there is no A in B. For example, claiming that no mobile phones in the room are on when there are no mobile phones in the room at all.
