Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-08-05T17:35:11-05:00

====== Juego de Cartas ======
Created lunes 05 agosto 2019

Es un juego de estrategia basado en cartas y recursos en el que varios jugadores combaten con ejércitos.

Cómo es el juego.

Mezcla de Magic y Super Triumpf

Los jugadores comienzan con n recursos
Los jugadores van a ronda de estrategia
	Los jugadores pueden gastar esos recursos en: 
	* Construir cuarteles -> Infantería
	* Construir un laboratorio de I+D -> Tecnologias
	* Inteligencia -> Trampas y Estrategia
	* Embajada -> Diplomacia, Negociación, Conseguir recursos extra
	* ~~Construir marina -> Soldado de marina~~
	* ~~Construir fuerza aérea -> Piloto~~
	* Construir un taller de artillería -> Armas pesadas
	El primer jugador que termine declara guerra
Entran en Fase de ataque
Para atacar se deben apostar recursos el que más apueste ataca
Al atacar la carta se manda al mazo de descanso
las cartas asesinadas van de nuevo al respectivo mazo general.
Cuando se terminen los ataques se vuelve a la ronda de estrategia con otra vez n recursos mas los que se ganaron los jugadores en sus ataques

Pasos a seguir

[ ] Hacer las cartas de cuarteles
[ ] Hacer las cartas de recursos
[ ] hacer las cartas de infantería


--------------------

Tener todo en un solo mazo
Reducir el precio en la primera ronda de estrategia


Modo de juego

Hay un mazo para todos los jugadores.

Las cartas son

Embajada
Laboratorio
Hospital
Inteligencia
Ministerio de Defensa

N Cartas de Soldado

Las cartas se disponen en una cuadrícula de 2 Filas x 3 Columnas

La primera fila representa el campo de batalla, aquí es dónde se ponen las cartas de soldado.
La segunda fila representa los edificios. Aquí se pueden poner las cartas de tipo Cuartel, Laboratorio, Embajada, Hospital, Inteligencia o Ministerio de Defensa.
Al lado izquierdo de la ficha anterior se encuentra la prisión militar.
Al lado derecho de la última fila se encuentra la zona de descanso.

¿Cómo se juega?

Los 4 jugadores se reparten los cuatro palacios al azar.
Se ponen las barreras y el palacio en su lugar.
Cada jugador toma 7 cartas.
Se apuesta el turno y gana el que apueste más recursos.

Disposición del juego



===== Turno =====

El ganador de la apuesta toma una carta.
Puede poner una carta en cualquiera de las posiciones disponibles para construir un edificio o directamente atacar una base enemiga.
Para hacer un ataque se compara una de las habilidades de la carta que va a atacar contra la misma habilidad de la carta que va a defender. (Super Triumpf)

==== Objeción de Conciencia ====

El juego está hecho normalmente jugarse a la ciega, es decir, con las cartas boca abajo y hay una gran posibilidad de que los jugadores estén mintiendo al decir sus estadísticas, por lo que si la otra persona piensa que el otro está mintiendo puede retarlo a mostrar su carta:

Si el jugador que reta demuestra que el otro está mintiendo: la carta pasa a ser de su posesión.

Si el jugador que ha sido retado dice la verdad, la carta mas adelantada de su contrincante pasa a ser de su posesión.

Las habilidades que cada soldado va a tener son:

Hablidad			Activa		Contra - Estadística contra la cuál se va a comparar

* {{./pasted_image016.png?width=24}}	Tipo de Arma		Pasiva		{{./pasted_image021.png?width=24}}
* {{./pasted_image001.png?width=24}} 	Disparo			Activa		{{./pasted_image014.png?width=24}}	{{./pasted_image004.png?width=24}}	{{./pasted_image018.png?width=24}}	
* {{./pasted_image023.png?width=24}}	Ataque Furtivo		Activa		{{./pasted_image021.png?width=24}}	{{./pasted_image015.png?width=24}}	{{./pasted_image012.png?width=24}}	
* {{./pasted_image015.png?width=24}} 	Ataque Cuerpo a cuerpo	Activa		{{./pasted_image012.png?width=24}}	{{./pasted_image004.png?width=24}}	
* {{./pasted_image006.png?width=24}}	Observación		Activa		{{./pasted_image017.png?width=24}}	{{./pasted_image014.png?width=24}}	
* {{./pasted_image017.png?width=24}} 	Sigilo			Activa		{{./pasted_image006.png?width=24}}
* {{./pasted_image014.png?width=24}}	Cobertura		Pasiva
* {{./pasted_image012.png?width=24}}	Fuerza			Pasiva
* {{./pasted_image004.png?width=24}} 	Reacción		Pasiva
* {{./pasted_image018.png?width=24}} 	Velocidad		Pasiva
* {{./pasted_image021.png?width=24}}	Distancia		Contextual

{{./pasted_image024.png?width=64}}
Hay tres tipos de distancia:
- Corta distancia: 
- Mediana distancia: 
- Larga distancia: 


Los jugadores tienen tres niveles de alcance/distancia: Corta y larga. Esto les permite usar los valores de distancia y tipo de arma.


Gana el que tenga mayor valor.
La carta a la que uno le gana se va a al mazo de la prisión de uno.
la carta que uno gasta va al mazo de descanso.

En cada turno

Se pone una carta en la mesa
Se saca una carta del mazo

¿Cómo bajar un edificio?

Se baja en posición horizontal y boca abajo ya que funciona como una carta trampa

¿Cómo bajar una carta?

Se baja en posición vertical y, dependiendo de si se quiere poner en modo sigilo o no, se pone boca arriba o boca abajo.

 primera posición en la que se pone una carta es en larga distancia. Se pueden apilar cartas para ir acercándose al rango cercano. Las cartas solo pueden disparar segun el rango que proporcionan sus armas.
[*] Hacer dos cartas de prueba con distintas estadísticas.



--------------------

===== Notas =====

* La distribución de las cartas puede ser:
	* 18 de corto alcance
	* 10 de mediano alcance
	* 2 de largo alcance
	* 5 cartas de Embajada: 2 + 1 + 1  
	* 5 cartas de Laboratorio: 2 + 1 + 1
	* 5 cartas de Hospital: 2 + 1 + 1
	* 5 cartas de Inteligencia: 2 + 1 + 1
	* 5 cartas de Ministerio de Defensa: 2 + 1 + 1

Cartas de Embajada
- Cese al Fuego: Frena un ataque enemigo.
- Canje Humanitario: Cambia un soldado por otro que esté en el campo de batalla.
- Liberación: Un adversario devuelve una carta de la prisión.

Cartas de Laboratorio

- Camuflaje: Le dá a un soldado Sigilo de 10 por una ronda.
- Minas: Mata al soldado enemigo que lo ataque.
- Arma Biológica: Mata un soldado de cada equipo enemigo.

Cartas de Hospital

- Primeros auxilios: Evita que manden a un soldado a la pila de descarte.
- Recuperación: Devuelve una carta de de la pila de descarte al mazo.
- En Plena Forma: Sumar +2 temporal a las características pasivas de una carta.

Cartas de Inteligencia

- Revelar Información: Deja descubiertas las estadísticas de una carta.
- Espía: Una carta del enemigo para a ser de la mano del jugador.
- Señuelo: Al ser declarado el ataque la carta atacada puede moverse de su posición actual.

Cartas de Ministerio de Defensa

- Impuesto de guerra: Permite bajar otro soldado extra.
- Operación de rescate: Tomar una carta de preferencia del mazo de un enemigo.
- Repatriación: Un soldado que se encuentre en prisión entra al campo de batalla.

--------------------

Correcciones al juego:

Mecánicas
Redacción

Reducir las estadísticas
No se utilizó sigilo

Vista, escucha y sigilo se utilizaron por fuerza mayor
Disparo y puñal malayo
Límites de estadísticas

Reevaluar los rangos no están haciendo tanto efecto

No hay suficientes cartas de ataques de ataque que amerite el rango

Las habilidades de los edificios son muy pasivas
Para cada habilidad buena una contra

Minas de diferentes valores

Meterse al otro lado mata el rango

Equipamento básico

Arma
chaleco
cuchillo

Observar
escuchar
esconderse

siempre está escondido

Habilidades de los edificios los enemigos no puede escuchar para no revelar la estadística

Invadir

Espacio relativo.


